# Maintainer: Carlo Landmeter <clandmeter@alpinelinux.org>
# Contributor: Łukasz Jendrysik <scadu@yandex.com>
pkgname=sqlite-tcl
pkgver=3.40.0
pkgrel=0
pkgdesc="Sqlite Tcl Extension Architecture (TEA)"
url="https://www.sqlite.org/"
arch="all"
license="blessing"
makedepends="readline-dev tcl-dev sqlite-dev libtool autoconf automake"
subpackages="$pkgname-doc"
options="!check" # no testsuite from upstream

# compute _ver
_a=${pkgver%%.*}
_b=${pkgver#$_a.}
_b=${_b%%.*}
_c=${pkgver#$_a.$_b.}
_c=${_c%%.*}
case $pkgver in
	*.*.*.*)_d=${pkgver##*.};;
	*.*.*)	_d=0;;
esac
[ $_b -lt 10 ] && _b=0$_b
[ $_c -lt 10 ] && _c=0$_c
[ $_d -lt 10 ] && _d=0$_d
_ver=${_a}${_b}${_c}$_d

# these variables depend on _ver being set
source="https://www.sqlite.org/2022/sqlite-autoconf-$_ver.tar.gz"
builddir="$srcdir/sqlite-autoconf-$_ver/tea"

prepare() {
	default_prepare

	if [ -f "$startdir"/../sqlite/APKBUILD ]; then
		(
		_tclver=$pkgver
		. "$startdir"/../sqlite/APKBUILD
		if [ "$_tclver" != "$pkgver" ]; then
			die "sqlite version mismatch ($_tclver != $pkgver)"
		fi
		)
	fi
}

build() {
	./configure \
		--build="$CBUILD" \
		--host="$CHOST" \
		--prefix=/usr \
		--with-system-sqlite \
		--enable-threads
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm644 license.terms \
		"$pkgdir"/usr/share/licenses/$pkgname/license.terms
}

sha512sums="
175271e620dd947df2ca63c3ea6cff80e16f8f808a3ea7f6700282768adc2d61ac375d42b2064c73eec849c67307ac5291fdadc3059861caa015ac3ac7a480e4  sqlite-autoconf-3400000.tar.gz
"
