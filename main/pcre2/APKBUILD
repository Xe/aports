# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=pcre2
pkgver=10.41
pkgrel=1
pkgdesc="Perl-compatible regular expression library"
url="https://pcre.org/"
arch="all"
license="BSD-3-Clause"
depends_dev="
	libedit-dev
	zlib-dev
	"
makedepends="$depends_dev"
subpackages="
	$pkgname-dev
	$pkgname-doc
	$pkgname-tools
	libpcre2-16:_libpcre
	libpcre2-32:_libpcre
	"
source="https://github.com/PhilipHazel/pcre2/releases/download/pcre2-$pkgver/pcre2-$pkgver.tar.bz2"

# secfixes:
#   10.40-r0:
#     - CVE-2022-1586
#     - CVE-2022-1587

build() {
	# Note: Forced -O3 is recommended (needed?) for Julia.
	./configure \
		CFLAGS="$CFLAGS -O3" \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--docdir=/usr/share/doc/$pkgname-$pkgver \
		--htmldir=/usr/share/doc/$pkgname-$pkgver/html \
		--enable-pcre2-16 \
		--enable-pcre2-32 \
		--enable-pcre2grep-libz \
		--enable-pcre2test-libedit \
		--with-match-limit-depth=8192 \
		--enable-jit
	make
}

check() {
	./RunTest
	./pcre2_jit_test
}

package() {
	make DESTDIR="$pkgdir" install
}

_libpcre() {
	local bits="${subpkgname##*-}"
	pkgdesc="PCRE2 with $bits bit character support"

	amove usr/lib/libpcre2-$bits.so*
}

tools() {
	pkgdesc="Auxiliary utilities for PCRE2"

	amove usr/bin
}

sha512sums="
328f331a56f152424f6021b37f8dcf660842c55d43ff39f1b49115f0d05ed651d0bbb66b43c0ed61d65022030615768b92ce5e6218a54e4e17152ec473cca68d  pcre2-10.41.tar.bz2
"
