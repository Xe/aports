# Contributor: Andy Hawkins <andy@gently.org.uk>
# Maintainer: Andy Hawkins <andy@gently.org.uk>
pkgname=py3-tidyexc
pkgver=0.9.0
pkgrel=4
pkgdesc="An exception class inspired by the tidyverse style guide."
url="https://pypi.org/project/tidyexc/"
arch="noarch"
license="MIT"
makedepends="
	py3-gpep517
	py3-flit-core
	"
checkdepends="
	py3-pytest
	"
source="https://files.pythonhosted.org/packages/source/t/tidyexc/tidyexc-$pkgver.tar.gz
	flit-core.patch
	"
builddir="$srcdir/tidyexc-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	PY_IGNORE_IMPORTMISMATCH=1 PYTHONPATH="$PWD" pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/tidyexc*.whl
}

sha512sums="
6b4787ef7dd36a2de0aea719f899a9376505334998f0a245d1dd6196ffc276bf92cf081ce9298c47ed9f67cb65fe1f46228c85db90cfe9ceb59e6e6a696c7397  tidyexc-0.9.0.tar.gz
797302081b65bc1dad525503e1bb81ae4885c20cb0ed0275cb15dc4ea50276cea219ef58fc7846add1343bfa2fa5e9bfb091d64e7c60ebb6ab0056d9893f0188  flit-core.patch
"
