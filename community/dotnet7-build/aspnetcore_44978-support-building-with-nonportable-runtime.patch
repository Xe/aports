From bc1c32a4b5af0075d0caa3ad71012f439305c026 Mon Sep 17 00:00:00 2001
From: Tom Deseyn <tom.deseyn@gmail.com>
Date: Tue, 13 Sep 2022 10:51:19 +0200
Subject: [PATCH 1/4] source-build: support building aspnetcore using
 non-portable runtime packages.

Currently source-build performs 'runtime-portable' build that produces 'linux-{arch}'
packages that are used when building ASP.NET Core.

With this change, we can use the non-portable packages that are produced by the
source-build 'runtime' build, and eliminate the 'runtime-portable' build.
---
 Directory.Build.props                             |  2 ++
 eng/Common.props                                  |  2 ++
 eng/Dependencies.props                            |  2 ++
 .../GenerateFiles/Directory.Build.targets.in      |  9 +++++++++
 .../Driver/Wasm.Performance.Driver.csproj         |  2 +-
 .../src/Microsoft.AspNetCore.App.Runtime.csproj   | 15 ++++++++-------
 src/Tools/Directory.Build.targets                 |  2 +-
 7 files changed, 25 insertions(+), 9 deletions(-)

diff --git a/src/aspnetcore/Directory.Build.props b/src/aspnetcore/Directory.Build.props
index 1230281ba953..95a35a753eb8 100644
--- a/src/aspnetcore/Directory.Build.props
+++ b/src/aspnetcore/Directory.Build.props
@@ -174,6 +174,8 @@
       freebsd-x64
     </SupportedRuntimeIdentifiers>
 
+    <SupportedRuntimeIdentifiers Condition=" '$(PortableBuild)' != 'true' ">$(SupportedRuntimeIdentifiers);$(TargetRuntimeIdentifier)</SupportedRuntimeIdentifiers>
+
     <!-- Playwright provides binaries for Windows (x86 and x64), macOS (x64) and Linux (x64, non musl). We can't use it on other architectures. -->
     <IsPlaywrightAvailable Condition="'$(TargetOsName)' == 'linux-musl' OR ('$(TargetArchitecture)' != 'x86' AND '$(TargetArchitecture)' != 'x64')">false</IsPlaywrightAvailable>
     <IsPlaywrightAvailable Condition="'$(IsPlaywrightAvailable)' == ''">true</IsPlaywrightAvailable>
diff --git a/src/aspnetcore/eng/Common.props b/src/aspnetcore/eng/Common.props
index a9a69bde9f22..d12f15da7917 100644
--- a/src/aspnetcore/eng/Common.props
+++ b/src/aspnetcore/eng/Common.props
@@ -6,6 +6,8 @@
     <TargetOsName Condition=" '$(TargetOsName)' == '' AND $([MSBuild]::IsOSPlatform('FreeBSD'))">freebsd</TargetOsName>
     <TargetArchitecture Condition="'$(TargetArchitecture)' == ''">x64</TargetArchitecture>
     <TargetRuntimeIdentifier Condition="'$(TargetRuntimeIdentifier)' == ''">$(TargetOsName)-$(TargetArchitecture)</TargetRuntimeIdentifier>
+    <PortableBuild Condition="'$(PortableBuild)' == ''">true</PortableBuild>
+    <DefaultAppHostRuntimeIdentifier Condition=" '$(PortableBuild)' != 'true' ">$(TargetRuntimeIdentifier)</DefaultAppHostRuntimeIdentifier>
   </PropertyGroup>
 
   <PropertyGroup Condition=" '$(BuildAllProjects)' == 'true' ">
diff --git a/src/aspnetcore/eng/Dependencies.props b/src/aspnetcore/eng/Dependencies.props
index c1b35e485328..adaecbb64c21 100644
--- a/src/aspnetcore/eng/Dependencies.props
+++ b/src/aspnetcore/eng/Dependencies.props
@@ -106,6 +106,7 @@ and are generated based on the last package release.
     <LatestPackageReference Include="Microsoft.NETCore.App.Runtime.linux-musl-arm" />
     <LatestPackageReference Include="Microsoft.NETCore.App.Runtime.linux-musl-arm64" />
     <LatestPackageReference Include="Microsoft.NETCore.App.Runtime.freebsd-x64" />
+    <LatestPackageReference Include="Microsoft.NETCore.App.Runtime.$(TargetRuntimeIdentifier)" Condition=" '$(PortableBuild)' != 'true' " />
 
     <!-- Crossgen2 compiler -->
     <LatestPackageReference Include="Microsoft.NETCore.App.Crossgen2.osx-x64" />
@@ -121,6 +122,7 @@ and are generated based on the last package release.
     <LatestPackageReference Include="Microsoft.NETCore.App.Crossgen2.win-arm" />
     <LatestPackageReference Include="Microsoft.NETCore.App.Crossgen2.win-arm64" />
     <LatestPackageReference Include="Microsoft.NETCore.App.Crossgen2.freebsd-x64" />
+    <LatestPackageReference Include="Microsoft.NETCore.App.Crossgen2.$(TargetRuntimeIdentifier)" Condition=" '$(PortableBuild)' != 'true' " />
   </ItemGroup>
 
   <ItemGroup Label=".NET team dependencies (Non-source-build)" Condition="'$(DotNetBuildFromSource)' != 'true'">
diff --git a/src/aspnetcore/eng/tools/GenerateFiles/Directory.Build.targets.in b/src/aspnetcore/eng/tools/GenerateFiles/Directory.Build.targets.in
index 3da7ea2b44d3..fe11e80b276c 100644
--- a/src/aspnetcore/eng/tools/GenerateFiles/Directory.Build.targets.in
+++ b/src/aspnetcore/eng/tools/GenerateFiles/Directory.Build.targets.in
@@ -56,18 +56,27 @@
       <DefaultRuntimeFrameworkVersion Condition=" ('$(IsServicingBuild)' != 'true' OR '$(NETCoreSdkVersion)' == '7.0.100-rtm.22478.12') AND
           '%(TargetFramework)' == '${DefaultNetCoreTargetFramework}' AND
           '$(TargetLatestDotNetRuntime)' != 'false' ">${MicrosoftNETCoreAppRuntimeVersion}</DefaultRuntimeFrameworkVersion>
+      <RuntimePackRuntimeIdentifiers Condition=" '$(PortableBuild)' != 'true' ">$(TargetRuntimeIdentifier)</RuntimePackRuntimeIdentifiers>
     </KnownFrameworkReference>
 
     <KnownAppHostPack Update="Microsoft.NETCore.App">
       <AppHostPackVersion
         Condition=" '%(TargetFramework)' == '${DefaultNetCoreTargetFramework}' ">${MicrosoftNETCoreAppRuntimeVersion}</AppHostPackVersion>
+      <AppHostRuntimeIdentifiers Condition=" '$(PortableBuild)' != 'true' ">$(TargetRuntimeIdentifier)</AppHostRuntimeIdentifiers>
     </KnownAppHostPack>
 
     <KnownRuntimePack Update="Microsoft.NETCore.App">
       <LatestRuntimeFrameworkVersion
         Condition=" '%(TargetFramework)' == '${DefaultNetCoreTargetFramework}' ">${MicrosoftNETCoreAppRuntimeVersion}</LatestRuntimeFrameworkVersion>
+      <AppHostRuntimeIdentifiers Condition=" '$(PortableBuild)' != 'true' ">$(TargetRuntimeIdentifier)</AppHostRuntimeIdentifiers>
     </KnownRuntimePack>
 
+    <KnownCrossgen2Pack Update="Microsoft.NETCore.App.Crossgen2" Condition=" '$(PortableBuild)' != 'true' ">
+      <Crossgen2PackVersion
+          Condition=" '%(TargetFramework)' == '${DefaultNetCoreTargetFramework}' ">${MicrosoftNETCoreAppRuntimeVersion}</Crossgen2PackVersion>
+      <Crossgen2RuntimeIdentifiers>$(TargetRuntimeIdentifier)</Crossgen2RuntimeIdentifiers>
+    </KnownCrossgen2Pack>
+
     <!-- Use the just-built ASP.NET Core shared framework if available except when building product code in servicing. -->
     <KnownFrameworkReference Update="Microsoft.AspNetCore.App" Condition=" $(UpdateAspNetCoreKnownFramework) ">
       <LatestRuntimeFrameworkVersion
diff --git a/src/aspnetcore/src/Components/benchmarkapps/Wasm.Performance/Driver/Wasm.Performance.Driver.csproj b/src/aspnetcore/src/Components/benchmarkapps/Wasm.Performance/Driver/Wasm.Performance.Driver.csproj
index bd51f667807f..91692198e32f 100644
--- a/src/aspnetcore/src/Components/benchmarkapps/Wasm.Performance/Driver/Wasm.Performance.Driver.csproj
+++ b/src/aspnetcore/src/Components/benchmarkapps/Wasm.Performance/Driver/Wasm.Performance.Driver.csproj
@@ -8,7 +8,7 @@
     <!-- WebDriver is not strong-named, so this test project cannot be strong named either. -->
     <SignAssembly>false</SignAssembly>
     <IsTestAssetProject>true</IsTestAssetProject>
-    <RuntimeIdentifier>linux-x64</RuntimeIdentifier>
+    <RuntimeIdentifier Condition=" '$(DotNetBuildFromSource)' != 'true' ">linux-x64</RuntimeIdentifier>
     <Nullable>annotations</Nullable>
   </PropertyGroup>
 
diff --git a/src/aspnetcore/src/Framework/App.Runtime/src/Microsoft.AspNetCore.App.Runtime.csproj b/src/aspnetcore/src/Framework/App.Runtime/src/Microsoft.AspNetCore.App.Runtime.csproj
index bf90848a34bd..c4e04fa432a5 100644
--- a/src/aspnetcore/src/Framework/App.Runtime/src/Microsoft.AspNetCore.App.Runtime.csproj
+++ b/src/aspnetcore/src/Framework/App.Runtime/src/Microsoft.AspNetCore.App.Runtime.csproj
@@ -96,18 +96,19 @@ This package is an internal implementation of the .NET Core SDK and is not meant
     <Crossgen2ToolFileName Condition=" '$(TargetOsName)' == 'win' ">$(Crossgen2ToolFileName).exe</Crossgen2ToolFileName>
 
     <!-- E.g. "PkgMicrosoft_NETCore_App_Runtime_win-x64" (set in obj/Microsoft.AspNetCore.App.Runtime.csproj.nuget.g.props). -->
-    <RuntimePackageRootVariableName>PkgMicrosoft_NETCore_App_Runtime_$(RuntimeIdentifier)</RuntimePackageRootVariableName>
+    <RuntimePackageRootVariableName>PkgMicrosoft_NETCore_App_Runtime_$(RuntimeIdentifier.Replace('.', '_'))</RuntimePackageRootVariableName>
 
     <!--
       Determine the crossgen2 package path property name. Special case linux-musl-arm and linux-musl-arm64 because they
       are built on an Ubuntu container with cross compilation tools. linux-musl-x64 is built in an alpine container.
       Special case the crossgen2 package reference on Windows to avoid the x86 package when building in Visual Studio.
     -->
-    <BuildOsName>$(TargetOsName)</BuildOsName>
-    <BuildOsName Condition="'$(TargetOsName)' == 'linux-musl' and '$(TargetArchitecture)'!='x64'">linux</BuildOsName>
-    <Crossgen2BuildArchitecture Condition=" '$(BuildOsName)' == 'win' ">x64</Crossgen2BuildArchitecture>
+    <Crossgen2OsName>$(TargetOsName)</Crossgen2OsName>
+    <Crossgen2OsName Condition="'$(TargetOsName)' == 'linux-musl' and '$(TargetArchitecture)'!='x64'">linux</Crossgen2OsName>
+    <Crossgen2OsName Condition=" '$(PortableBuild)' != 'true' ">$(TargetRuntimeIdentifier.Substring(0,$(TargetRuntimeIdentifier.IndexOf('-'))))</Crossgen2OsName>
+    <Crossgen2BuildArchitecture Condition=" '$(Crossgen2OsName)' == 'win' ">x64</Crossgen2BuildArchitecture>
     <Crossgen2BuildArchitecture Condition=" '$(Crossgen2BuildArchitecture)' == '' ">$(BuildArchitecture)</Crossgen2BuildArchitecture>
-    <Crossgen2PackageRootVariableName>PkgMicrosoft_NETCore_App_Crossgen2_$(BuildOsName)-$(Crossgen2BuildArchitecture)</Crossgen2PackageRootVariableName>
+    <Crossgen2PackageRootVariableName>PkgMicrosoft_NETCore_App_Crossgen2_$(Crossgen2OsName.Replace('.', '_'))-$(Crossgen2BuildArchitecture)</Crossgen2PackageRootVariableName>
 
     <AssetTargetFallback>$(AssetTargetFallback);native,Version=0.0</AssetTargetFallback>
 
@@ -138,7 +139,7 @@ This package is an internal implementation of the .NET Core SDK and is not meant
       $(GeneratePathProperty) and hacks in the _ExpandRuntimePackageRoot target work around the gaps.
     -->
     <Reference Condition="'$(CrossgenOutput)' == 'true'"
-        Include="Microsoft.NETCore.App.Crossgen2.$(BuildOsName)-$(Crossgen2BuildArchitecture)"
+        Include="Microsoft.NETCore.App.Crossgen2.$(Crossgen2OsName)-$(Crossgen2BuildArchitecture)"
         ExcludeAssets="All"
         PrivateAssets="All"
         GeneratePathProperty="true" />
@@ -444,7 +445,7 @@ This package is an internal implementation of the .NET Core SDK and is not meant
     <PropertyGroup>
       <!--
         Handle different names for the target OS on the crossgen2 command line. This often matches neither
-        $(TargetOsName) nor $(BuildOsName).
+        $(TargetOsName) nor $(Crossgen2OsName).
       -->
       <Crossgen2TargetOs>$(TargetOsName)</Crossgen2TargetOs>
       <Crossgen2TargetOs Condition="'$(TargetOsName)' == 'win'">windows</Crossgen2TargetOs>
diff --git a/src/aspnetcore/src/Tools/Directory.Build.targets b/src/aspnetcore/src/Tools/Directory.Build.targets
index 854f90ab5c20..438ea3da045d 100644
--- a/src/aspnetcore/src/Tools/Directory.Build.targets
+++ b/src/aspnetcore/src/Tools/Directory.Build.targets
@@ -1,7 +1,7 @@
 <Project>
   <PropertyGroup Condition=" '$(PackAsTool)' == 'true' ">
     <!-- Microsoft tool packages are required to target both x64 and x86. -->
-    <PackAsToolShimRuntimeIdentifiers Condition=" '$(IsShippingPackage)' == 'true' ">win-x64;win-x86</PackAsToolShimRuntimeIdentifiers>
+    <PackAsToolShimRuntimeIdentifiers Condition=" '$(IsShippingPackage)' == 'true' AND '$(DotNetBuildFromSource)' != 'true' ">win-x64;win-x86</PackAsToolShimRuntimeIdentifiers>
     <!-- None of the tool projects are project reference providers. -->
     <IsProjectReferenceProvider>false</IsProjectReferenceProvider>
     <!--

From c14e15308ff0a7c19c15481a323f2c1e3269558c Mon Sep 17 00:00:00 2001
From: Tom Deseyn <tom.deseyn@gmail.com>
Date: Tue, 13 Sep 2022 14:17:35 +0200
Subject: [PATCH 2/4] Invert PortableBuild checks.

---
 Directory.Build.props                                     | 2 +-
 eng/Common.props                                          | 2 +-
 eng/Dependencies.props                                    | 4 ++--
 eng/tools/GenerateFiles/Directory.Build.targets.in        | 8 ++++----
 .../src/Microsoft.AspNetCore.App.Runtime.csproj           | 2 +-
 5 files changed, 9 insertions(+), 9 deletions(-)

diff --git a/src/aspnetcore/Directory.Build.props b/src/aspnetcore/Directory.Build.props
index 95a35a753eb8..d79b636c698c 100644
--- a/src/aspnetcore/Directory.Build.props
+++ b/src/aspnetcore/Directory.Build.props
@@ -174,7 +174,7 @@
       freebsd-x64
     </SupportedRuntimeIdentifiers>
 
-    <SupportedRuntimeIdentifiers Condition=" '$(PortableBuild)' != 'true' ">$(SupportedRuntimeIdentifiers);$(TargetRuntimeIdentifier)</SupportedRuntimeIdentifiers>
+    <SupportedRuntimeIdentifiers Condition=" '$(PortableBuild)' == 'false' ">$(SupportedRuntimeIdentifiers);$(TargetRuntimeIdentifier)</SupportedRuntimeIdentifiers>
 
     <!-- Playwright provides binaries for Windows (x86 and x64), macOS (x64) and Linux (x64, non musl). We can't use it on other architectures. -->
     <IsPlaywrightAvailable Condition="'$(TargetOsName)' == 'linux-musl' OR ('$(TargetArchitecture)' != 'x86' AND '$(TargetArchitecture)' != 'x64')">false</IsPlaywrightAvailable>
diff --git a/src/aspnetcore/eng/Common.props b/src/aspnetcore/eng/Common.props
index d12f15da7917..3dcca1c6b54d 100644
--- a/src/aspnetcore/eng/Common.props
+++ b/src/aspnetcore/eng/Common.props
@@ -7,7 +7,7 @@
     <TargetArchitecture Condition="'$(TargetArchitecture)' == ''">x64</TargetArchitecture>
     <TargetRuntimeIdentifier Condition="'$(TargetRuntimeIdentifier)' == ''">$(TargetOsName)-$(TargetArchitecture)</TargetRuntimeIdentifier>
     <PortableBuild Condition="'$(PortableBuild)' == ''">true</PortableBuild>
-    <DefaultAppHostRuntimeIdentifier Condition=" '$(PortableBuild)' != 'true' ">$(TargetRuntimeIdentifier)</DefaultAppHostRuntimeIdentifier>
+    <DefaultAppHostRuntimeIdentifier Condition=" '$(PortableBuild)' == 'false' ">$(TargetRuntimeIdentifier)</DefaultAppHostRuntimeIdentifier>
   </PropertyGroup>
 
   <PropertyGroup Condition=" '$(BuildAllProjects)' == 'true' ">
diff --git a/src/aspnetcore/eng/Dependencies.props b/src/aspnetcore/eng/Dependencies.props
index adaecbb64c21..0d47cc0a111b 100644
--- a/src/aspnetcore/eng/Dependencies.props
+++ b/src/aspnetcore/eng/Dependencies.props
@@ -106,7 +106,7 @@ and are generated based on the last package release.
     <LatestPackageReference Include="Microsoft.NETCore.App.Runtime.linux-musl-arm" />
     <LatestPackageReference Include="Microsoft.NETCore.App.Runtime.linux-musl-arm64" />
     <LatestPackageReference Include="Microsoft.NETCore.App.Runtime.freebsd-x64" />
-    <LatestPackageReference Include="Microsoft.NETCore.App.Runtime.$(TargetRuntimeIdentifier)" Condition=" '$(PortableBuild)' != 'true' " />
+    <LatestPackageReference Include="Microsoft.NETCore.App.Runtime.$(TargetRuntimeIdentifier)" Condition=" '$(PortableBuild)' == 'false' " />
 
     <!-- Crossgen2 compiler -->
     <LatestPackageReference Include="Microsoft.NETCore.App.Crossgen2.osx-x64" />
@@ -122,7 +122,7 @@ and are generated based on the last package release.
     <LatestPackageReference Include="Microsoft.NETCore.App.Crossgen2.win-arm" />
     <LatestPackageReference Include="Microsoft.NETCore.App.Crossgen2.win-arm64" />
     <LatestPackageReference Include="Microsoft.NETCore.App.Crossgen2.freebsd-x64" />
-    <LatestPackageReference Include="Microsoft.NETCore.App.Crossgen2.$(TargetRuntimeIdentifier)" Condition=" '$(PortableBuild)' != 'true' " />
+    <LatestPackageReference Include="Microsoft.NETCore.App.Crossgen2.$(TargetRuntimeIdentifier)" Condition=" '$(PortableBuild)' == 'false' " />
   </ItemGroup>
 
   <ItemGroup Label=".NET team dependencies (Non-source-build)" Condition="'$(DotNetBuildFromSource)' != 'true'">
diff --git a/src/aspnetcore/eng/tools/GenerateFiles/Directory.Build.targets.in b/src/aspnetcore/eng/tools/GenerateFiles/Directory.Build.targets.in
index fe11e80b276c..30e25d4e68a5 100644
--- a/src/aspnetcore/eng/tools/GenerateFiles/Directory.Build.targets.in
+++ b/src/aspnetcore/eng/tools/GenerateFiles/Directory.Build.targets.in
@@ -56,22 +56,22 @@
       <DefaultRuntimeFrameworkVersion Condition=" ('$(IsServicingBuild)' != 'true' OR '$(NETCoreSdkVersion)' == '7.0.100-rtm.22478.12') AND
           '%(TargetFramework)' == '${DefaultNetCoreTargetFramework}' AND
           '$(TargetLatestDotNetRuntime)' != 'false' ">${MicrosoftNETCoreAppRuntimeVersion}</DefaultRuntimeFrameworkVersion>
-      <RuntimePackRuntimeIdentifiers Condition=" '$(PortableBuild)' != 'true' ">$(TargetRuntimeIdentifier)</RuntimePackRuntimeIdentifiers>
+      <RuntimePackRuntimeIdentifiers Condition=" '$(PortableBuild)' == 'false' ">$(TargetRuntimeIdentifier)</RuntimePackRuntimeIdentifiers>
     </KnownFrameworkReference>
 
     <KnownAppHostPack Update="Microsoft.NETCore.App">
       <AppHostPackVersion
         Condition=" '%(TargetFramework)' == '${DefaultNetCoreTargetFramework}' ">${MicrosoftNETCoreAppRuntimeVersion}</AppHostPackVersion>
-      <AppHostRuntimeIdentifiers Condition=" '$(PortableBuild)' != 'true' ">$(TargetRuntimeIdentifier)</AppHostRuntimeIdentifiers>
+      <AppHostRuntimeIdentifiers Condition=" '$(PortableBuild)' == 'false' ">$(TargetRuntimeIdentifier)</AppHostRuntimeIdentifiers>
     </KnownAppHostPack>
 
     <KnownRuntimePack Update="Microsoft.NETCore.App">
       <LatestRuntimeFrameworkVersion
         Condition=" '%(TargetFramework)' == '${DefaultNetCoreTargetFramework}' ">${MicrosoftNETCoreAppRuntimeVersion}</LatestRuntimeFrameworkVersion>
-      <AppHostRuntimeIdentifiers Condition=" '$(PortableBuild)' != 'true' ">$(TargetRuntimeIdentifier)</AppHostRuntimeIdentifiers>
+      <AppHostRuntimeIdentifiers Condition=" '$(PortableBuild)' == 'false' ">$(TargetRuntimeIdentifier)</AppHostRuntimeIdentifiers>
     </KnownRuntimePack>
 
-    <KnownCrossgen2Pack Update="Microsoft.NETCore.App.Crossgen2" Condition=" '$(PortableBuild)' != 'true' ">
+    <KnownCrossgen2Pack Update="Microsoft.NETCore.App.Crossgen2" Condition=" '$(PortableBuild)' == 'false' ">
       <Crossgen2PackVersion
           Condition=" '%(TargetFramework)' == '${DefaultNetCoreTargetFramework}' ">${MicrosoftNETCoreAppRuntimeVersion}</Crossgen2PackVersion>
       <Crossgen2RuntimeIdentifiers>$(TargetRuntimeIdentifier)</Crossgen2RuntimeIdentifiers>
diff --git a/src/aspnetcore/src/Framework/App.Runtime/src/Microsoft.AspNetCore.App.Runtime.csproj b/src/aspnetcore/src/Framework/App.Runtime/src/Microsoft.AspNetCore.App.Runtime.csproj
index c4e04fa432a5..ef07af11fece 100644
--- a/src/aspnetcore/src/Framework/App.Runtime/src/Microsoft.AspNetCore.App.Runtime.csproj
+++ b/src/aspnetcore/src/Framework/App.Runtime/src/Microsoft.AspNetCore.App.Runtime.csproj
@@ -105,7 +105,7 @@ This package is an internal implementation of the .NET Core SDK and is not meant
     -->
     <Crossgen2OsName>$(TargetOsName)</Crossgen2OsName>
     <Crossgen2OsName Condition="'$(TargetOsName)' == 'linux-musl' and '$(TargetArchitecture)'!='x64'">linux</Crossgen2OsName>
-    <Crossgen2OsName Condition=" '$(PortableBuild)' != 'true' ">$(TargetRuntimeIdentifier.Substring(0,$(TargetRuntimeIdentifier.IndexOf('-'))))</Crossgen2OsName>
+    <Crossgen2OsName Condition=" '$(PortableBuild)' == 'false' ">$(TargetRuntimeIdentifier.Substring(0,$(TargetRuntimeIdentifier.IndexOf('-'))))</Crossgen2OsName>
     <Crossgen2BuildArchitecture Condition=" '$(Crossgen2OsName)' == 'win' ">x64</Crossgen2BuildArchitecture>
     <Crossgen2BuildArchitecture Condition=" '$(Crossgen2BuildArchitecture)' == '' ">$(BuildArchitecture)</Crossgen2BuildArchitecture>
     <Crossgen2PackageRootVariableName>PkgMicrosoft_NETCore_App_Crossgen2_$(Crossgen2OsName.Replace('.', '_'))-$(Crossgen2BuildArchitecture)</Crossgen2PackageRootVariableName>

From 754c9302f46115eb5191a4e2f88bba1f1b0a6b00 Mon Sep 17 00:00:00 2001
From: Tom Deseyn <tom.deseyn@gmail.com>
Date: Thu, 15 Sep 2022 11:07:17 +0200
Subject: [PATCH 3/4] Revert name change.

---
 .../src/Microsoft.AspNetCore.App.Runtime.csproj    | 14 +++++++-------
 1 file changed, 7 insertions(+), 7 deletions(-)

diff --git a/src/aspnetcore/src/Framework/App.Runtime/src/Microsoft.AspNetCore.App.Runtime.csproj b/src/aspnetcore/src/Framework/App.Runtime/src/Microsoft.AspNetCore.App.Runtime.csproj
index ef07af11fece..efb1d7eccf1b 100644
--- a/src/aspnetcore/src/Framework/App.Runtime/src/Microsoft.AspNetCore.App.Runtime.csproj
+++ b/src/aspnetcore/src/Framework/App.Runtime/src/Microsoft.AspNetCore.App.Runtime.csproj
@@ -103,12 +103,12 @@ This package is an internal implementation of the .NET Core SDK and is not meant
       are built on an Ubuntu container with cross compilation tools. linux-musl-x64 is built in an alpine container.
       Special case the crossgen2 package reference on Windows to avoid the x86 package when building in Visual Studio.
     -->
-    <Crossgen2OsName>$(TargetOsName)</Crossgen2OsName>
-    <Crossgen2OsName Condition="'$(TargetOsName)' == 'linux-musl' and '$(TargetArchitecture)'!='x64'">linux</Crossgen2OsName>
-    <Crossgen2OsName Condition=" '$(PortableBuild)' == 'false' ">$(TargetRuntimeIdentifier.Substring(0,$(TargetRuntimeIdentifier.IndexOf('-'))))</Crossgen2OsName>
-    <Crossgen2BuildArchitecture Condition=" '$(Crossgen2OsName)' == 'win' ">x64</Crossgen2BuildArchitecture>
+    <BuildOsName>$(TargetOsName)</BuildOsName>
+    <BuildOsName Condition="'$(TargetOsName)' == 'linux-musl' and '$(TargetArchitecture)'!='x64'">linux</BuildOsName>
+    <BuildOsName Condition=" '$(PortableBuild)' == 'false' ">$(TargetRuntimeIdentifier.Substring(0,$(TargetRuntimeIdentifier.IndexOf('-'))))</BuildOsName>
+    <Crossgen2BuildArchitecture Condition=" '$(BuildOsName)' == 'win' ">x64</Crossgen2BuildArchitecture>
     <Crossgen2BuildArchitecture Condition=" '$(Crossgen2BuildArchitecture)' == '' ">$(BuildArchitecture)</Crossgen2BuildArchitecture>
-    <Crossgen2PackageRootVariableName>PkgMicrosoft_NETCore_App_Crossgen2_$(Crossgen2OsName.Replace('.', '_'))-$(Crossgen2BuildArchitecture)</Crossgen2PackageRootVariableName>
+    <Crossgen2PackageRootVariableName>PkgMicrosoft_NETCore_App_Crossgen2_$(BuildOsName.Replace('.', '_'))-$(Crossgen2BuildArchitecture)</Crossgen2PackageRootVariableName>
 
     <AssetTargetFallback>$(AssetTargetFallback);native,Version=0.0</AssetTargetFallback>
 
@@ -139,7 +139,7 @@ This package is an internal implementation of the .NET Core SDK and is not meant
       $(GeneratePathProperty) and hacks in the _ExpandRuntimePackageRoot target work around the gaps.
     -->
     <Reference Condition="'$(CrossgenOutput)' == 'true'"
-        Include="Microsoft.NETCore.App.Crossgen2.$(Crossgen2OsName)-$(Crossgen2BuildArchitecture)"
+        Include="Microsoft.NETCore.App.Crossgen2.$(BuildOsName)-$(Crossgen2BuildArchitecture)"
         ExcludeAssets="All"
         PrivateAssets="All"
         GeneratePathProperty="true" />
@@ -445,7 +445,7 @@ This package is an internal implementation of the .NET Core SDK and is not meant
     <PropertyGroup>
       <!--
         Handle different names for the target OS on the crossgen2 command line. This often matches neither
-        $(TargetOsName) nor $(Crossgen2OsName).
+        $(TargetOsName) nor $(BuildOsName).
       -->
       <Crossgen2TargetOs>$(TargetOsName)</Crossgen2TargetOs>
       <Crossgen2TargetOs Condition="'$(TargetOsName)' == 'win'">windows</Crossgen2TargetOs>
