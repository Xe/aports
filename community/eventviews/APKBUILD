# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=eventviews
pkgver=22.12.0
pkgrel=0
pkgdesc="Library for creating events"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> akonadi
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
license="GPL-2.0-or-later"
depends_dev="
	akonadi-calendar-dev
	akonadi-dev
	calendarsupport-dev
	kcalendarcore-dev
	kcalutils-dev
	kcodecs-dev
	kcompletion-dev
	kdiagram-dev
	kguiaddons-dev
	kholidays-dev
	ki18n-dev
	kiconthemes-dev
	kmime-dev
	kservice-dev
	libkdepim-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
makedepends="$depends_dev extra-cmake-modules"
source="https://download.kde.org/stable/release-service/$pkgver/src/eventviews-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
5762a3323e227470b8862ac24de2237e1a2d3feb393306179c061f893d02e4ee03ae3de24b246ea7cde507be167e7c3d15f1d165742cd363d9f32dcb5534efb5  eventviews-22.12.0.tar.xz
"
