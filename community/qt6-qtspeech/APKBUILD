# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qt6-qtspeech
pkgver=6.4.1
pkgrel=0
pkgdesc="Qt6 module to make text to speech and speech recognition easy"
url="https://qt.io/"
arch="all"
license="LGPL-2.1-only AND LGPL-3.0-only AND GPL-3.0-only AND Qt-GPL-exception-1.0"
depends_dev="
	alsa-lib-dev
	flite-dev
	qt6-qtdeclarative-dev
	qt6-qtmultimedia-dev
	"
makedepends="$depends_dev
	cmake
	perl
	samurai
	"
subpackages="$pkgname-dev"
options="!check" # No tests
builddir="$srcdir/qtspeech-everywhere-src-${pkgver/_/-}"

case $pkgver in
	*_alpha*|*_beta*|*_rc*) _rel=development_releases;;
	*) _rel=official_releases;;
esac

source="https://download.qt.io/$_rel/qt/${pkgver%.*}/${pkgver/_/-}/submodules/qtspeech-everywhere-src-${pkgver/_/-}.tar.xz"

build() {
	export CFLAGS="$CFLAGS -flto=auto"
	export CXXFLAGS="$CXXFLAGS -flto=auto"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
83c8612215690761c2b4150fc86e13fff547588c98a0f6d97f6ac9a7e2719f85f06c4739f0480f36a9df7805f945ee5e32007a5c52dc58c30632dc47cbd6a49f  qtspeech-everywhere-src-6.4.1.tar.xz
"
