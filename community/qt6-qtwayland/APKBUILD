# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qt6-qtwayland
pkgver=6.4.1
pkgrel=2
pkgdesc="Provides APIs for Wayland"
url="https://qt.io/"
arch="all"
license="LGPL-2.1-only AND LGPL-3.0-only AND GPL-3.0-only AND Qt-GPL-exception-1.0"
depends_dev="
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	vulkan-headers
	"
makedepends="$depends_dev
	cmake
	perl
	samurai
	"
subpackages="$pkgname-dev"
options="!check" # No tests
builddir="$srcdir/qtwayland-everywhere-src-${pkgver/_/-}"

# Make sure this package is installed when a Qt application is installed and a
# Wayland session is available on the system
# XXX: apk doesn't install_if here unless one of the things here is explicitly
# in world, so add alpine-base to make sure something here always is, so this
# installs as intended
install_if="wayland-libs-server qt6-qtbase-x11 alpine-base"

case $pkgver in
	*_alpha*|*_beta*|*_rc*) _rel=development_releases;;
	*) _rel=official_releases;;
esac

source="https://download.qt.io/$_rel/qt/${pkgver%.*}/${pkgver/_/-}/submodules/qtwayland-everywhere-src-${pkgver/_/-}.tar.xz"

build() {
	export CFLAGS="$CFLAGS -g1 -flto=auto"
	export CXXFLAGS="$CXXFLAGS -g1 -flto=auto"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6b238f269c77f86f31ef97a4f34d36ff26fd59d68cc8df98862658a972fdd3946dfb1301cdd310d5223c55c9ebb767fdb4c30793c5de6e1ba243c896b2964803  qtwayland-everywhere-src-6.4.1.tar.xz
"
