# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=syft
pkgver=0.62.3
pkgrel=1
pkgdesc="Generate a Software Bill of Materials (SBOM) from container images and filesystems"
url="https://github.com/anchore/syft"
license="Apache-2.0"
arch="all !armhf !armv7 !x86 !ppc64le !riscv64" # FTBFS on 32-bit arches, riscv64, ppc64le
makedepends="go"
source="https://github.com/anchore/syft/archive/v$pkgver/syft-$pkgver.tar.gz"
options="!check" # tests need docker

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -ldflags "
		-X github.com/anchore/syft/internal/version.version=$pkgver
		" \
		-o bin/syft ./cmd/syft
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/syft -t "$pkgdir"/usr/bin/
}

sha512sums="
981b9c6e9a34adfdafbb743f5e681649ed8eec3cb63fd1b2fc9ed4b864667abb28d945be4a9ceb3613fa25520b72bd68f5abf768ae0926c7a7955ee90a217f51  syft-0.62.3.tar.gz
"
