# Contributor: Rudolf Polzer <divVerent@gmail.com>
# Maintainer: Rudolf Polzer <divVerent@gmail.com>
pkgname=aaaaxy
pkgver=1.2.399
pkgrel=1
pkgdesc="A nonlinear puzzle platformer taking place in impossible spaces"
url="https://divVerent.github.io/aaaaxy/"
arch="all !s390x !armhf !armv7 !riscv64"
license="Apache-2.0"
makedepends="
	alsa-lib-dev
	go
	libx11-dev
	libxcursor-dev
	libxinerama-dev
	libxi-dev
	libxrandr-dev
	mesa-dev
"
checkdepends="xvfb-run mesa-dri-gallium"
source="
	https://github.com/divVerent/aaaaxy/archive/v$pkgver/aaaaxy-$pkgver.tar.gz
	https://github.com/divVerent/aaaaxy/releases/download/v$pkgver/sdl-gamecontrollerdb-for-aaaaxy-v$pkgver.zip
"
options="net"  # Needed for go mod download.

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

prepare() {
	default_prepare

	mv "$srcdir"/third_party/SDL_GameControllerDB/assets/input/* \
		third_party/SDL_GameControllerDB/assets/input/
	go mod download
}

build() {
	export AAAAXY_BUILD_USE_VERSION_FILE=true
	make BUILDTYPE=release
}

check() {
	xvfb-run sh scripts/regression-test-demo.sh aaaaxy \
		"on track for Any%, All Paths and No Teleports" \
		./aaaaxy benchmark.dem
}

package() {
	install -Dm644 io.github.divverent.aaaaxy.metainfo.xml \
		"$pkgdir"/usr/share/metainfo/io.github.divverent.aaaaxy.metainfo.xml
	install -Dm644 aaaaxy.png \
		"$pkgdir"/usr/share/icons/hicolor/128x128/apps/aaaaxy.png
	install -Dm644 aaaaxy.desktop \
		"$pkgdir"/usr/share/applications/aaaaxy.desktop
	install -Dm755 aaaaxy \
		"$pkgdir"/usr/bin/aaaaxy
}

sha512sums="
1030e6d1c7e6340e7a53d08fe38df99c19fb9ea58a21fc37af7d3ff50a612001f5cc763e812d5774eeff626edb1fa474fa71b36f116141283dffb775f5118932  aaaaxy-1.2.399.tar.gz
56c8dbcfd087866da2887055d60d4bc193fef208735ebd1073a7b794186c37fbfddcd1e47c9c2355786c517ba79e906f7e058c14c4128eb53ce990ba247e09a2  sdl-gamecontrollerdb-for-aaaaxy-v1.2.399.zip
"
