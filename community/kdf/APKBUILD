# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdf
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/system/org.kde.kdf"
pkgdesc="View disk usage"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knotifications-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdf-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d03163094d6153a6f43d4d0706dbb5863d707620848dd91dd0200a6ddd24065f1ec6ad9da9cff46f0ed6dd6e712a0e728fed85fa4d3346e16ab876ecfbf51928  kdf-22.12.0.tar.xz
"
