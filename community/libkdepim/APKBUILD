# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkdepim
pkgver=22.12.0
pkgrel=0
pkgdesc="Lib for common KDEPim apps"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> akonadi
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://community.kde.org/KDE_PIM"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
depends_dev="
	akonadi-contacts-dev
	akonadi-dev
	akonadi-search-dev
	kcmutils-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemviews-dev
	kjobwidgets-dev
	kldap-dev
	kwallet-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	samurai
	"
makedepends="$depends_dev
	extra-cmake-modules
	qt5-qttools-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkdepim-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_DESIGNERPLUGIN=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
18a958fbb2e48e1754617ee0af06af8dd2e43883b6a6dc38912c9813deaefb39751fcd7177052b50e8ea26fc16d4a4413ab839e745e1a3002cee86d48617a453  libkdepim-22.12.0.tar.xz
"
