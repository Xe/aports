# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=frameworkintegration
pkgver=5.100.0
pkgrel=0
pkgdesc="Framework providing components to allow applications to integrate with a KDE Workspace"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org"
license="LGPL-2.1-or-later"
depends_dev="
	appstream-dev
	kconfig-dev
	kconfigwidgets-dev
	ki18n-dev
	kiconthemes-dev
	knewstuff-dev
	knotifications-dev
	kpackage-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	samurai
	"
makedepends="$depends_dev
	extra-cmake-modules
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/frameworkintegration-$pkgver.tar.xz"
subpackages="$pkgname-dev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f99bfbb8476bd3b14c6611f8557b000257c1464b6c21dcbbb1e69f27e8371f64e45ea4f2817b8c3514de13c9e69888dbad905e7d3ed8f1b4cac82c3205f9a873  frameworkintegration-5.100.0.tar.xz
"
