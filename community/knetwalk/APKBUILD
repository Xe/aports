# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=knetwalk
pkgver=22.12.0
pkgrel=0
pkgdesc="Connect all the terminals to the server, in as few turns as possible"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/games/knetwalk/"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdegames-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/knetwalk-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
969a5a4fc2135e543d8d8a028e752b9a3f0f9855a5342a8bf95d4c43eec37ecf8d069bc8c04f81506d0cf3d42abec56d58ee9a4fea4ac7eb3ff6cde603285f21  knetwalk-22.12.0.tar.xz
"
