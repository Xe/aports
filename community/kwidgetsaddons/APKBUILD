# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwidgetsaddons
pkgver=5.100.0
pkgrel=0
pkgdesc="Addons to QtWidgets"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="GPL-2.0-only AND LGPL-2.1-only AND Unicode-DFS-2016"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="
	mesa-dri-gallium
	xvfb-run
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kwidgetsaddons-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# ktwofingertaptest, ktwofingerswipetest and and ksqueezedtextlabelautotest are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E '(ktooltipwidget|ktwofingertap|ktwofingerswipe|ksqueezedtextlabelauto)test'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
92762cbae607f79df87cf32bacf9e7db1cba8da9e27bb4b2988a5850a5fa70ffe1460f86908073a1d1712f1274ea4e50c07eed6301c09bf2dbbc2fb4a58fd965  kwidgetsaddons-5.100.0.tar.xz
"
