# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdialog
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/"
pkgdesc="A utility for displaying dialog boxes from shell scripts"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdbusaddons-dev
	kguiaddons-dev
	kiconthemes-dev
	kio-dev
	knotifications-dev
	ktextwidgets-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdialog-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a3536ea8be2d77de621d8bd1557dd18c6678bc71ed82171f289b182f4d9d9317f9c6d434ad45c8d9d52f75aeaddaac7d6154694a86eeb9bd595f564e069ab3da  kdialog-22.12.0.tar.xz
"
