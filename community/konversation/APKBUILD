# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=konversation
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://konversation.kde.org/"
pkgdesc="A user-friendly and fully-featured IRC client"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	karchive-dev
	kbookmarks-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kidletime-dev
	kio-dev
	kitemviews-dev
	knewstuff-dev
	knotifications-dev
	knotifyconfig-dev
	kparts-dev
	kwallet-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	phonon-dev
	qca-dev
	qt5-qtbase-dev
	qt5-qtmultimedia-dev
	samurai
	solid-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/konversation-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f8ab198c7a279e2865b4f0bec89cebbd9b8d9efdf5a4002e9a79159b420ba939618488227a124cb3350cb39be47b5cbaa9f39e96c3442d091d2b121373e5e2fb  konversation-22.12.0.tar.xz
"
