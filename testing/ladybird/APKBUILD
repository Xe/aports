# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=ladybird
pkgver=0_git20221107
pkgrel=0
_gitrev=0faa0d411c722e1551e73930ed57911b8eb2a57e
_serenitygitrev=7eed3dab5d77116ba9749ceb66e5d4e998db7c22
_tzdbver="2022f" # serenity/Meta/CMake/time_zone_data.cmake
_ucdver="15.0.0" # serenity/Meta/CMake/unicode_data.cmake
_cldrver="42.0.0" # serenity/Meta/CMake/locale_data.cmake
pkgdesc="Ladybird Web Browser using the SerenityOS LibWeb engine"
url="https://github.com/awesomekling/ladybird"
# s390x: FTBFS on big-endian
# riscv64: fails to build
# ppc64le: #error "Unknown architecture. Don't know whether pointers need to be sign-extended."
arch="all !s390x !riscv64 !ppc64le"
license="BSD-2-Clause"
makedepends="
	bsd-compat-headers
	chrpath
	cmake
	qt6-qtbase-dev
	qt6-qtwayland-dev
	samurai
	"
source="https://github.com/awesomekling/ladybird/archive/$_gitrev/ladybird-$pkgver.tar.gz
	https://github.com/SerenityOS/serenity/archive/$_serenitygitrev/serenity-$_serenitygitrev.tar.gz

	https://data.iana.org/time-zones/releases/tzdata$_tzdbver.tar.gz
	ucd-$_ucdver.zip::https://www.unicode.org/Public/$_ucdver/ucd/UCD.zip
	https://github.com/unicode-org/cldr-json/releases/download/$_cldrver/cldr-$_cldrver-json-modern.zip

	resource-paths.patch
	no-werror.patch.serenity
	missing-cdefs.patch.serenity
	arch-string.patch.serenity
	"
builddir="$srcdir/ladybird-$_gitrev"
options="!check" # no tests from upstream

prepare() {
	default_prepare

	# move vendored files to the correct places
	mkdir -p build/TZDB
	cp "$srcdir"/tzdata$_tzdbver.tar.gz build/TZDB/tzdb.tar.gz
	echo $_tzdbver > build/TZDB/version.txt

	mkdir -p build/UCD
	cp "$srcdir"/ucd-$_ucdver.zip build/UCD/UCD.zip
	echo $_ucdver > build/UCD/version.txt

	mkdir -p build/CLDR
	cp "$srcdir"/cldr-$_cldrver-json-modern.zip build/CLDR/cldr.zip
	echo $_cldrver > build/CLDR/version.txt

	# apply patches for the Serenity tree
	cd "$srcdir"/serenity-$_serenitygitrev
	for i in $source; do
		case $i in
		*.patch.serenity) msg $i; patch -p1 -i "$srcdir"/$i ;;
		esac
	done
}

build() {
	cmake -G Ninja -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DSERENITY_SOURCE_DIR="$srcdir"/serenity-$_serenitygitrev
	cmake --build build

	# fix rpath
	chrpath -r /usr/lib/ladybird \
		build/ladybird \
		build/WebContent/WebContent
	find build/_deps/lagom-build -name '*.so' -exec \
		chrpath -r /usr/lib/ladybird {} \;
}

package() {
	install -Dm755 build/ladybird "$pkgdir"/usr/bin/ladybird
	install -Dm755 build/WebContent/WebContent "$pkgdir"/usr/bin/WebContent

	find build/_deps/lagom-build -name '*.so' -o -name '*.so.*' -exec \
		install -Dm755 {} -t "$pkgdir"/usr/lib/ladybird \;

	cd "$srcdir"/serenity-$_serenitygitrev/Base/res
	install -Dm644 icons/16x16/app-browser.png \
		"$pkgdir"/usr/share/icons/hicolor/16x16/apps/ladybird.png
	install -Dm644 \
		icons/16x16/go-back.png \
		icons/16x16/go-forward.png \
		icons/16x16/go-home.png \
		icons/16x16/reload.png \
		-t "$pkgdir"/usr/share/ladybird/res/icons/16x16
	install -Dm644 html/error.html \
		-t "$pkgdir"/usr/share/ladybird/res/html
	install -Dm644 themes/Default.ini \
		-t "$pkgdir"/usr/share/ladybird/res/themes
	install -Dm644 fonts/* \
		-t "$pkgdir"/usr/share/ladybird/res/fonts
}

sha512sums="
a0bcb3fc290b8b5b786cb1e74716793b2be0c2357ed85f6e29d8b789a19836031fad7ea96d370d2a46a6ba95e86c57fb6837ea316e685eaa5bc7b2ed0e8ad3a0  ladybird-0_git20221107.tar.gz
94cdb9879bb9e99b645e22ce0251f5e2bd4adaa938f5f2ae121dcc60b8dca265c0e655b0eacccd02a55edc5765cf698459b318e969bdede1bf079eda42d9b3c1  serenity-7eed3dab5d77116ba9749ceb66e5d4e998db7c22.tar.gz
72d05d05be999075cdf57b896c0f4238b1b862d4d0ed92cc611736592a4ada14d47bd7f0fc8be39e7938a7f5940a903c8af41e87859482bcfab787d889d429f6  tzdata2022f.tar.gz
442a99694fccf0d1ff6f6236bec2cff7408eef4d9dba575ba4f5dc45cebf4d2b1b30334ef8a843887a0f410b24e0f79f7f30f06a195659def998875981604a6a  ucd-15.0.0.zip
0c84958d065d1636531953424f707380281886c8dc5f1107df09312a79b2bcece0bc00f5897de2b56f02818e9692641c960cd3703956d48cc0d62f214c2fdb06  cldr-42.0.0-json-modern.zip
9027eaf5a3aea916c37b76e8750cce7c8caf6723c1166709ddfa7a27e1727ad07700890c955275f6bdacc1faa669352ea59feeec51b953d537d2b50512343e80  resource-paths.patch
033c34f8fde269b5774bbd361faac1284cd23aa01bdbd5a1b0e022069bb15a4e9436fa5c9f0238573724320dc40011dcc3b4272fc7e61ea055766682eb4862ff  no-werror.patch.serenity
03e193a440d05ab4455b6ec17fe00daaa3b9fec9157cb1f1559e84367a1c50c8ca321d55d63d8c27691b54a40e6367058f15cd47b1f67a5ed4aeff5e4d40f404  missing-cdefs.patch.serenity
30ca3e4d136c3dd7dae30ea15b129b096a1d61629c9ea88a1c72ce0c9f2254ab2e946e1fef8fa1925a374577137cd4fbf7a881bcf22f0ef6568ffd2f864de80c  arch-string.patch.serenity
"
