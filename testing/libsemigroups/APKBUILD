# Contributor: Grigory Kirillov <txgk@bk.ru>
# Maintainer: Grigory Kirillov <txgk@bk.ru>
pkgname=libsemigroups
pkgver=2.4.0
pkgrel=0
pkgdesc="Library for computing semigroups and monoids"
url="https://github.com/libsemigroups/libsemigroups"
arch="all"
license="GPL-3.0-or-later"
makedepends="eigen-dev fmt-dev"
subpackages="$pkgname-static $pkgname-dev"
source="https://github.com/libsemigroups/libsemigroups/releases/download/v$pkgver/libsemigroups-$pkgver.tar.gz"

build() {
	export CXXFLAGS="${CXXFLAGS/-Os/-O3}" # gotta go fast

	./configure \
		--target="$CTARGET" \
		--build="$CBUILD" \
		--host="$CHOST" \
		--prefix=/usr \
		--enable-eigen \
		--enable-fmt \
		--with-external-eigen \
		--with-external-fmt
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
cf94173a4a60006bd6b55ba34f45adb56224eddac0b61e51c158e9d06cb0ee8df9e119f988b0eb1b8c1cf81672e33c1a5dc33168c729ae2d8f1c42510522c9c9  libsemigroups-2.4.0.tar.gz
"
