# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: TBK <alpine@jjtc.eu>
pkgname=php7-pecl-protobuf
_extname=protobuf
pkgver=3.21.10
pkgrel=0
pkgdesc="PHP 7.4 extension: Google's language-neutral, platform-neutral, extensible mechanism for serializing structured data."
url="https://pecl.php.net/package/protobuf"
arch="all"
license="BSD-3-Clause"
_phpv=7
_php=php$_phpv
depends="$_php-common"
makedepends="$_php-dev"
source="php-pecl-$_extname-$pkgver.tgz::https://pecl.php.net/get/$_extname-$pkgver.tgz"
builddir="$srcdir/$_extname-$pkgver"
provides="php7-protobuf=$pkgver-r$pkgrel" # for backward compatibility
replaces="php7-protobuf" # for backward compatibility

build() {
	phpize$_phpv
	./configure --prefix=/usr --with-php-config=php-config$_phpv
	make
}

check() {
	# Test suite is not a part of pecl release.
	$_php -d extension=modules/$_extname.so --ri $_extname
}

package() {
	make INSTALL_ROOT="$pkgdir" install
	local _confdir="$pkgdir"/etc/$_php/conf.d
	install -d $_confdir
	echo "extension=$_extname" > $_confdir/$_extname.ini
}

sha512sums="
720430ce48f5638154088b0616066d97ca1ee61eea2e1f44b810b582dfc616d3789761564ee76918fa14a2333e407db79e27131c95e5c1ab7047b175baf9fd70  php-pecl-protobuf-3.21.10.tgz
"
