# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=pulumi-language-yaml
pkgver=1.0.3
pkgrel=1
pkgdesc="Infrastructure as Code SDK (YAML language provider)"
url="https://pulumi.com/"
arch="all"
license="Apache-2.0"
depends="pulumi"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/pulumi/pulumi-yaml/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/pulumi-yaml-$pkgver"
# Requires PULUMI_ACCESS_TOKEN for tests to be run
options="!check"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"
export GOFLAGS="$GOFLAGS -modcacherw"

build() {
	go build -v \
		-ldflags "-X github.com/pulumi/pulumi-yaml/pkg/version.Version=v$pkgver" \
		-o $pkgname \
		./cmd/pulumi-language-yaml/
}

package() {
	install -Dm755 $pkgname -t "$pkgdir"/usr/bin/
}

sha512sums="
772453248db33a8afb89a9afde3f8696e097040ed9fb1f52fe008b7b89548001b42103b7f53fe6f3e8566c5ad89de2d21d3169b5ec657b749920f363fe6f3836  pulumi-language-yaml-1.0.3.tar.gz
"
